﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[RequireComponent (typeof ( AudioSource ))]
public class AudioBank : Bank 
{
	[HideInInspector] public AudioClip 			QueuedClip = null;
	[HideInInspector] public AudioClip 			PlayingClip = null;
	[HideInInspector] public bool				FlagToStopClip;

	public void SetQueuedClip( int inClipIndex )
	{
		QueuedClip = _AudioClips[ inClipIndex ];
	}

	public void QueueClipToStop()
	{
		Debug.Log ( " --- QueueClipToStop()");
		FlagToStopClip = true;
		QueuedClip = null;
	}

	public void PlayQueuedClip() 
	{
		Debug.Log ( " --- PlayQueuedClip()");
		PlayingClip = QueuedClip;

		audio.Stop();
		
		audio.clip = PlayingClip;
		audio.loop = false;
		audio.Play();
	}

	/*
	public void PlayClip( int inClipIndex ) 
	{
		_PlayingClip = _AudioClips[ inClipIndex ];

		audio.clip = _PlayingClip;
		audio.loop = true;
	}*/

	public void StopClip()
	{
		Debug.Log( "---------- StopClip()  " + PlayingClip );
		FlagToStopClip = false;
		audio.Stop();
		audio.clip = null;
		PlayingClip = null;
		QueuedClip = null;
	}

	public void SetVolume( float inVolume )
	{
		audio.volume = inVolume;
	}

	public float GetClipLength( int inClipIndex )
	{
		return _AudioClips[ inClipIndex ].samples;
	}

	public float GetCurrentClipPosition()
	{
		return audio.timeSamples;
	}

	[SerializeField] private List<AudioClip>		_AudioClips = new List<AudioClip>();
}