﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class CardboardController : MonoBehaviour 
{
	public static bool		Initialized;
	public static string	CurrentLevelName = "Noontime";
	public static bool		IsFlying = false;

	private void Start()
	{
		if( ! Initialized )
		{
			Initialized = true;
			
			Screen.sleepTimeout = SleepTimeout.NeverSleep;
			
			_LevelButtons.SetActive( false );
			
			if( ! MagnetSensor.EventIsSet )
			{
				MagnetSensor.EventIsSet = true;
				
				MagnetSensor.OnCardboardTrigger += MagnetClicked;
			}
			
			//only load level on start when in Loader scene
			if( Application.loadedLevelName.ToLower() == "loader" )
			{
				Application.LoadLevel ( CurrentLevelName );
			}
		}

		RefreshButtons();
	}

	private void MagnetClicked()
	{
		//Debug.Log( "Magnet Triggered" );
		
		if( IsFlying && ! _HitSomething )
		{
			_AscentSpeed = 0;
		}
		
		if( IsFlying && _RayCastHittingGroundPlane )
		{
			_AscentSpeed = -.1f;
			
			if( _MoveClip != null )
			{
				AudioSource.PlayClipAtPoint( _MoveClip, transform.position, _MoveVolume );
			}
			
			return;
		}
		
		if( _RayCastHittingFlyPlane )
		{
			if( ! IsFlying )
			{
				_HUD.SetActive(false);
				transform.localPosition += new Vector3( 0, 1, 0 );//pop off the ground to avoid landing collision from being triggered
				IsFlying = true;
				
				_FlyingSpeed = 0;
			}
			
			SendMessage( "TurnOffGravity" );
			
			_AscentSpeed = Mathf.Max( _AscentSpeed, .1f );
			//_FlyPlane.SetActive( false );
			
			if( _MoveClip != null )
			{
				AudioSource.PlayClipAtPoint( _MoveClip, transform.position, _MoveVolume );
			}

			return;
		}

		if( _HighlightedLevelName != string.Empty && _HighlightedLevelName != CurrentLevelName )
		{
			CurrentLevelName = _HighlightedLevelName;

			RefreshButtons();

			if( _ButtonClickClip != null )
			{
				AudioSource.PlayClipAtPoint( _ButtonClickClip, transform.position, _ButtonClickVolume );
			}

			Application.LoadLevel ( CurrentLevelName );
		}
		else
		{
			if( _MoveClip != null )
			{
				AudioSource.PlayClipAtPoint( _MoveClip, transform.position, _MoveVolume );
			}

			//increment move speed
			++ _MoveSpeedIndex;
			_MoveSpeedIndex %= _MoveSpeeds.Count;//loop around to zero
		}
	}

	private void RefreshButtons()
	{
		_Buttons[0].collider.enabled = CurrentLevelName != "Noontime";
		_Selectors[0].SetActive( CurrentLevelName == "Noontime" );
		_Buttons[1].collider.enabled = CurrentLevelName != "Sunsettime";
		_Selectors[1].SetActive( CurrentLevelName == "Sunsettime" );
		_Buttons[2].collider.enabled = CurrentLevelName != "Nighttime";
		_Selectors[2].SetActive( CurrentLevelName == "Nighttime" );
	}
	
	private void Update() 
	{
		//move character controller
		if( IsFlying )
		{
			//cheapo accelerate
			if( _FlyingSpeed < _MaxFlyingSpeed )
			{
				_FlyingSpeed += .2f;
			}
			
			//increase ascent or descent
			if( _AscentSpeed > 0 && _AscentSpeed < _MaxAscentSpeed )
			{
				_AscentSpeed += .01f;
			}
			else if( _AscentSpeed < 0 && _AscentSpeed > -1 * _MaxAscentSpeed )
			{
				_AscentSpeed -= .01f;
			}
			
			Vector3 moveVector = new Vector3( _CameraTransform.forward.x * _FlyingSpeed, _AscentSpeed, _CameraTransform.forward.z * _FlyingSpeed );
			_CharacterController.Move( moveVector );
		}
		else
		{
			_CharacterController.SimpleMove( _CameraTransform.forward * _MoveSpeeds[ _MoveSpeedIndex ] );
		}

		if( Input.GetKeyDown( KeyCode.T ) ) 
		{
			MagnetClicked();
		}

		RaycastHit hit;

		Physics.Raycast( _CameraTransform.position, _CameraTransform.forward, out hit, 900 );

		_HitSomething = hit.collider != null && hit.collider.gameObject != null;

		bool hitHud = false;
		bool hitIcon = false;

		if( _HitSomething )
		{
			//Debug.Log( "hit.collider.gameObject.tag = " + hit.collider.gameObject.tag );
			_RayCastHittingFlyPlane = hit.collider.gameObject.tag == "FlyPlane";
			_RayCastHittingGroundPlane = hit.collider.gameObject.tag == "Ground";

			hitHud = hit.collider.gameObject.tag == "HUD";
			hitIcon = hit.collider.gameObject.tag == "LevelIcon";

			if( ! hitIcon && ! hitHud )
			{
				if( _LevelButtons.activeSelf )
				{
					HideHUD();
				}

				return;
			}

			if( ! IsFlying ) //don't show HUD when looking down when flying
			{
				if( ! _LevelButtons.activeSelf && ( hitIcon || hitHud ) ) //do it only once
				{
					_LevelButtons.SetActive( true );
					
					Vector3 euler = _CameraTransform.rotation.eulerAngles;
					_HUD.transform.rotation = Quaternion.identity;
					_HUD.transform.Rotate( 0, euler.y, 0 );
				}
				
				//deselect if not same icon
				if( hit.collider.gameObject.name != _HighlightedLevelName )
				{
					//deselect last icon
					if( _HighlightedLevelIcon != null )
					{
						_HighlightedLevelIcon.transform.localScale = new Vector3( .005f, .005f, .005f );
						
						_HighlightedLevelIcon = null;
					}
					
					_HighlightedLevelName = string.Empty;
				}
				
				
				if( hitIcon )
				{
					if( hit.collider.gameObject.name != _HighlightedLevelName )
					{
						_HighlightedLevelName = hit.collider.gameObject.name;
						_HighlightedLevelIcon = hit.collider.gameObject;
						
						//select it
						_HighlightedLevelIcon.transform.localScale = new Vector3( .0062f, .0062f, .0062f );
						
						if( _ButtonHoverClip != null )
						{
							AudioSource.PlayClipAtPoint( _ButtonHoverClip, transform.position, _ButtonHoverVolume );
						}
					}
				}
			}
		}
		else
		{
			HideHUD();
		}
	}

	public void OnControllerColliderHit( ControllerColliderHit inHit ) 
	{
		if( IsFlying )
		{
			if( inHit.collider.tag == "Ground" )
			{
				IsFlying = false;
				_AscentSpeed = 0;
				//_FlyPlane.SetActive( true );
				_HUD.SetActive(true);
				
				SendMessage( "TurnOnGravity" );
			}
			else if( _AscentSpeed > 0 && inHit.collider.tag == "GlassCeiling" )
			{
				_AscentSpeed = 0;
				transform.localPosition -= new Vector3( 0, 1, 0 );//pop off ceiling so raycast won't hit it
			}
		}
	}

	private void HideHUD()
	{
		_LevelButtons.SetActive( false );
		
		//deselect last icon (just in case)
		if( _HighlightedLevelIcon != null )
		{
			_HighlightedLevelIcon.transform.localScale = new Vector3( .005f, .005f, .005f );
			
			_HighlightedLevelIcon = null;
		}
		
		_HighlightedLevelName = string.Empty;
	}

	private float 									_FlyingSpeed = 0;
	private float									_AscentSpeed = 0;
	private GameObject								_HighlightedLevelIcon = null;
	private string									_HighlightedLevelName = string.Empty;
	private int										_MoveSpeedIndex = 0;
	private bool									_RayCastHittingFlyPlane = false;
	private bool									_RayCastHittingGroundPlane = false;
	private bool									_HitSomething = false;

	[SerializeField] private float 					_MaxFlyingSpeed = 10;
	[SerializeField] private float					_MaxAscentSpeed = 1;

	[SerializeField] private List<float>			_MoveSpeeds = new List<float>();

	[SerializeField] private GameObject[]			_Buttons = new GameObject[3];
	[SerializeField] private GameObject[]			_Selectors = new GameObject[3];
	
	[SerializeField] private GameObject				_HUD = null;
	[SerializeField] private GameObject				_LevelButtons = null;
	[SerializeField] private GameObject				_FlyPlane = null;
	[SerializeField] private Transform				_CameraTransform = null;
	[SerializeField] private CharacterController	_CharacterController = null;

	[Header("Audio Clips")]
	[SerializeField] private AudioClip				_ButtonHoverClip = null;
	[SerializeField] private float					_ButtonHoverVolume = 1;
	[SerializeField] private AudioClip				_ButtonClickClip = null;
	[SerializeField] private float					_ButtonClickVolume = 1;
	[SerializeField] private AudioClip				_MoveClip = null;
	[SerializeField] private float					_MoveVolume = 1;
}
