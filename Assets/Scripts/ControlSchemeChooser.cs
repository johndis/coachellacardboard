﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ControlSchemeChooser : MonoBehaviour 
{
	[Header("Check to Simulate Cardboard")]
	public bool ShowDiveCameraInEditor = false;
	
	private void Start()
	{
		#if UNITY_EDITOR
		if( ShowDiveCameraInEditor )
		{
			_EditorCamera.SetActive( false );
			_DiveCamera.SetActive( true );
			_MouseLook.enabled = false;
			_MouseLook.SendMessage( "SetDisabled" );
		}
		else
		{
			_EditorCamera.SetActive( true );
			_DiveCamera.SetActive( false );
			_MouseLook.enabled = true;
			_MouseLook.SendMessage( "SetEnabled" );
		}
		#endif

		#if ! UNITY_EDITOR
		_EditorCamera.SetActive( false );
		_DiveCamera.SetActive( true );
		_MouseLook.enabled = false;
		_MouseLook.SendMessage( "SetDisabled" );
		#endif
	}
	
	[SerializeField] GameObject _EditorCamera = null;
	[SerializeField] GameObject _DiveCamera = null;
	[SerializeField] MouseLook _MouseLook = null;
}
