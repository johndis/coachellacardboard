﻿using UnityEngine;
using System.Collections;

public class BeatCounter : MonoBehaviour 
{
	public static BeatCounter Instance = null;

	private void Awake() 
	{
		Instance = this;
	}

	private void Start()
	{
		Reset();
	}

	public void Reset()
	{
		_CurrentBeatSprite = null;
		_CurrentBarSprite = null;

		for( int i = 0; i < _Beats.Length; ++i )
		{
			_Beats[i].color = _OffColor;
		}

		for( int i = 0; i < _Bars.Length; ++i )
		{
			_Bars[i].color = _BarOffColor;
		}
	}

	public void SetBeat( int inBeat )
	{
		if( _CurrentBeatSprite != null )
		{
			_CurrentBeatSprite.color = _OffColor;
		}

		_CurrentBeatSprite = _Beats[ inBeat ];
		_CurrentBeatSprite.color = _OnColor;


		if( (float) inBeat % 4 == 0 )
		{
			if( _CurrentBarSprite != null )
			{
				_CurrentBarSprite.color = _BarOffColor;
			}

			int barNum = Mathf.FloorToInt( (float) inBeat / 4 );

			_CurrentBarSprite = _Bars[ barNum ];
			_CurrentBarSprite.color = _BarOnColor;

			if( _ShouldPlayMetronomeClick )
			{
				audio.PlayOneShot( _MetronomeClick, .8f );
			}
		}
		else
		{
			if( _ShouldPlayMetronomeClick )
			{
				audio.PlayOneShot( _MetronomeClick, .3f );
			}
		}
	}
	
	private UISprite			_CurrentBeatSprite = null;
	private UISprite			_CurrentBarSprite = null;

	[SerializeField] bool		_ShouldPlayMetronomeClick;
	[SerializeField] UISprite[]	_Bars = new UISprite[4];
	[SerializeField] UISprite[]	_Beats = new UISprite[16];

	[SerializeField] Color		_OffColor;
	[SerializeField] Color		_OnColor;
	[SerializeField] Color		_AccentColor;
	[SerializeField] Color		_BarOffColor;
	[SerializeField] Color		_BarOnColor;
	[SerializeField] AudioClip	_MetronomeClick = null;
}
