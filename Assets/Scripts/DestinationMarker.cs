﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class DestinationMarker : MonoBehaviour 
{
	public void Show( MoveFPCToPoint inPlayer, Vector3 inPosition )
	{
		_Player = inPlayer;

		transform.position = inPosition;

		gameObject.SetActive( true );
	}

	private void OnTriggerEnter( Collider inCollider )
	{
		if( inCollider.gameObject.tag == "FPC" && _Player != null )
		{
			_Player.StopMovement();

			_Player = null;

			gameObject.SetActive( false );
		}
	}

	private MoveFPCToPoint _Player = null;
}
