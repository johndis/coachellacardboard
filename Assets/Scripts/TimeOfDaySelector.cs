﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class TimeOfDaySelector : MonoBehaviour 
{
	public static TimeOfDaySelector Instance = null;

	private void Awake() 
	{
		Instance = this;
		DontDestroyOnLoad (gameObject);
	}

	private void Start()
	{
		_Buttons[0].enabled = false;
		_Selectors[0].SetActive( true );
		_Buttons[1].enabled = true;
		_Selectors[1].SetActive( false );
		_Buttons[2].enabled = true;
		_Selectors[2].SetActive( false );

		HideSequencerButton();

		_CurrentLevel = "Noontime";
		Application.LoadLevel ( _CurrentLevel );
	}
	
	public void HandleSunButtonPress()
	{
		_Buttons[0].enabled = false;
		_Selectors[0].SetActive( true );
		_Buttons[1].enabled = true;
		_Selectors[1].SetActive( false );
		_Buttons[2].enabled = true;
		_Selectors[2].SetActive( false );

		HideSequencerButton();

		_CurrentLevel = "Noontime";
		Application.LoadLevel ( _CurrentLevel );
	}
	
	public void HandleSunsetButtonPress()
	{
		_Buttons[0].enabled = true;
		_Selectors[0].SetActive( false );
		_Buttons[1].enabled = false;
		_Selectors[1].SetActive( true );
		_Buttons[2].enabled = true;
		_Selectors[2].SetActive( false );

		HideSequencerButton();

		_CurrentLevel = "Sunsettime";
		Application.LoadLevel ( _CurrentLevel );
	}
	
	public void HandleMoonButtonPress()
	{
		_Buttons[0].enabled = true;
		_Selectors[0].SetActive( false );
		_Buttons[1].enabled = true;
		_Selectors[1].SetActive( false );
		_Buttons[2].enabled = false;
		_Selectors[2].SetActive( true );

		ShowSequencerButton();

		_CurrentLevel = "Nighttime";
		Application.LoadLevel ( _CurrentLevel );
	}

	public void HandleFerrisWheelButtonPress()
	{
		FerrisWheelEnterZone f = GameObject.FindObjectOfType<FerrisWheelEnterZone>();
		if( f != null )
		{
			f.HandleButtonPress();
		}

		//temp
		_LevelButtons.SetActive( !_LevelButtons.activeSelf );
	}

	public void HandleSequencerButtonPress()
	{
		Sequencer s = GameObject.FindObjectOfType<Sequencer>();
		if( s != null )
		{
			s.Toggle();

			_LevelButtons.SetActive( s.IsHidden );
		}
	}

	private void ShowSequencerButton()
	{
		_SequencerButton.gameObject.SetActive( true );
	}

	private void HideSequencerButton()
	{
		_SequencerButton.gameObject.SetActive( false );
	}

	public void ShowFerrisWheelButton()
	{
		_FerrisWheelButton.gameObject.SetActive( true );
	}
	
	public void HideFerrisWheelButton()
	{
		_FerrisWheelButton.gameObject.SetActive( false );
	}

	private string								_CurrentLevel = string.Empty;

	[SerializeField ] private UIButton			_SequencerButton = null;
	[SerializeField ] private UIButton			_FerrisWheelButton = null;
	[SerializeField ] private GameObject		_LevelButtons = null;
	[SerializeField ] private UIButton[]		_Buttons = new UIButton[3];
	[SerializeField ] private GameObject[]		_Selectors = new GameObject[3];
}