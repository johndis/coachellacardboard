﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class MoveFPCToPoint : MonoBehaviour 
{
	private void Start()
	{
		Screen.sleepTimeout = SleepTimeout.NeverSleep;

		_DestinationMarker.gameObject.SetActive( false );

		if( ! MagnetSensor.EventIsSet )
		{
			MagnetSensor.EventIsSet = true;

			MagnetSensor.OnCardboardTrigger += BeginMoveToPoint;

			//Debug.Log( "OnEnable" );
		}

		//CardboardMagnetSensor.SetEnabled( true );

		//_MagnetClick = new MagnetClick();
		//_MagnetClick.Init();
	}

	private void OnDisable()
	{
		MagnetSensor.OnCardboardTrigger -= BeginMoveToPoint;
	}

	public void StopMovement()
	{
		//_CharacterController.SimpleMove()
	}

	private void BeginMoveToPoint()
	{
		Debug.Log( "Magnet Triggered" );

		//_Particle.SetActive( ! _Particle.activeSelf );

		AudioSource.PlayClipAtPoint( _TriggerClip, transform.position, 1 );

		StopMovement();

		_DestinationMarker.Show( this, _CrossHairs.transform.position );
		_MoveVector = _CameraTransform.forward * 10;

		//do raycast
	}

	private void Update() 
	{
		if( Input.GetKeyDown( KeyCode.T ) ) 
		{
			BeginMoveToPoint();
		}

		RaycastHit hit;

		if( Physics.Raycast( _CameraTransform.position, _CameraTransform.forward, out hit, 100 ) ) 
		{
			if( hit.collider.tag == "GroundPlane" )
			{
				_CrossHairs.SetActive( true );
				_CrossHairs.transform.position = hit.point + new Vector3( 0, .1f, 0 );
			}
			else
			{
				_CrossHairs.SetActive( false );
			}
		}
		else
		{
			_CrossHairs.SetActive( false );
		}

		if( _DestinationMarker.gameObject.activeSelf )
		{
			_CharacterController.SimpleMove( _MoveVector );
			//_CharacterController.SimpleMove( _CameraTransform.forward * 10 );
		}
	}

	private Vector3									_MoveVector = Vector3.zero;
	private MagnetClick								_MagnetClick = null;

	[SerializeField] private AudioClip				_TriggerClip = null;
	[SerializeField] private GameObject				_CrossHairs = null;
	[SerializeField] private DestinationMarker		_DestinationMarker = null;
	[SerializeField] private Transform				_CameraTransform = null;
	[SerializeField] private CharacterController	_CharacterController = null;
}
